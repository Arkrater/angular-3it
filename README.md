## Tecnologias Utilizadas
-- Frontend -- 
1. Angular 12.0.4
2. Bootstrap 5 
3. Node 14.17.0
4. IDE - Atom + plugins a elección de uno

## Instalacion
1. Descargar el proyecto 3it
2. cd ../ruta/proyecto
3. npm install
4. ng serve
5. Ingresar a la App a la URL localhost:4200, debe ser este puerto por el CrossOrigin con Springboot

## Version
0.0.1

## Motivo del proyecto
Este proyecto se realizó con el objetivo de entregar un producto MVP para la entrevista técnica en 3IT, 
donde el desafío fue desarrollar una pagina de encuestas con integración 
de Angular como Frontend y Java Springboot como Backend con almacenado en una base de datos en memoria (H2)