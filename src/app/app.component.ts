import { Component, OnInit } from '@angular/core';
import { SurveyService } from './services/survey.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'Encuesta Musical';
  mensajeBienvenida: string = 'Bievenidos a esta pequeña encuesta sobre tus gustos musicales';

  constructor( private surveyService: SurveyService) { }

  ngOnInit(): void {
  }
}
