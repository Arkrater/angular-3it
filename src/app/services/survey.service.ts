import { Injectable } from '@angular/core';
import { Survey } from './survey';

@Injectable({
  providedIn: 'root'
})
export class SurveyService {

  surveys: Survey[] = [
    {
      question: 'Que escuchas cuando estas solo?',
      answer: [
        { option: 'Rock'},
        { option: 'Jazz'},
        { option: 'Pop'},
        { option: 'Otro'}
      ]
    },
    {
      question: 'Que escuchas cuando te sientes triste?',
      answer: [
        { option: 'Rock'},
        { option: 'Jazz'},
        { option: 'Pop'},
        { option: 'Otro'}
      ]
    },
    {
      question: 'Que escuchas cuando algo que realmente querias te resulta bien?',
      answer: [
        { option: 'Rock'},
        { option: 'Jazz'},
        { option: 'Pop'},
        { option: 'Otro'}
      ]
    },
    {
      question: 'Que escuchas con tus amistades cuando estan compartiendo buenos momentos?',
      answer: [
        { option: 'Rock'},
        { option: 'Jazz'},
        { option: 'Pop'},
        { option: 'Otro'}
      ]
    },
  ]



  constructor() { }

  getSurveys(){
    return this.surveys;
  }

}
