import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RespuestaService {

  private baseUrl = 'http://localhost:8080/api/respuestas';


  constructor(private http: HttpClient) { }

  enviarRespuesta(respuesta: Object): Observable<Object> {
  return this.http.post(`${this.baseUrl}` + `/create`, respuesta);
}
}
