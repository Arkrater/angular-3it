import { Component, OnInit } from '@angular/core';
import { Survey } from '../services/survey';
import { SurveyService } from '../services/survey.service';
import { Usuario } from '../entities/usuario';
import { Respuesta } from '../entities/respuesta';
import { UsuarioService } from '../services/usuario.service';
import { RespuestaService } from '../services/respuesta.service';

@Component({
  selector: 'app-survey',
  templateUrl: './survey.component.html',
  styleUrls: ['./survey.component.css']
})
export class SurveyComponent implements OnInit {

  usuario: Usuario = new Usuario();
  respuesta: Respuesta = new Respuesta();
  emailRespuesta!: string;
  habilitar: boolean = true;

  surveys: Survey[]= [];

  currentSurvey = 0;
  answerSelected = false;
  result = false;
  inicio: boolean = true;

  constructor(private usuarioService: UsuarioService, private surveyService: SurveyService, private respuestaService: RespuestaService) { }

  ngOnInit(): void {

    this.surveys = this.surveyService.getSurveys();
  }

  save() {
    this.emailRespuesta = this.usuario.email;
    this.usuarioService.crearUsuario(this.usuario)
    .subscribe(data => console.log(data), error => console.log(error));
    this.usuario = new Usuario();
  }

  onSubmit() {
    this.inicio = false;
    this.save();
  }

  onAnswer(option: string){
    console.log();
    this.respuesta.pregunta = this.surveys[this.currentSurvey].question;
    this.respuesta.respuesta = option;
    this.respuesta.email = this.emailRespuesta;
    this.respuestaService.enviarRespuesta(this.respuesta)
    .subscribe(data => console.log(data), error => console.log(error));

    this.answerSelected = true;
    setTimeout(() => {
      this.currentSurvey++;
      this.answerSelected = false;
    }, 750)


  }

  finishSurvey(){
    this.inicio = true;
    this.result = true;
    console.log(this.result + " ha terminado");
    setTimeout(() => {
      window.location.reload();
    }, 6000)
  }

}
